import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [
    new Recipe('A Test Recipe', 'This is description for recipe', 
    'https://get.pxhere.com/photo/vegetarian-foods-healthy-dish-food-cuisine-ingredient-Stinky-tofu-curry-produce-vegetarian-food-tofu-meat-recipe-thai-food-massaman-curry-coddle-thai-curry-chinese-food-vietnamese-food-stew-1588461.jpg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
